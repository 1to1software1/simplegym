##Simple Gym: Privacy policy

Welcome to a gym app for Android!

This app does not collect any personally identifiable information. All data (for example, app preferences, alarms, and any data entered by you) is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it. 
No data that you enter is sent outside your device at any time.

If you find any security vulnerability that has been inadvertently caused, or have any question regarding how the app manages your information, please send me an email and we will try to help you.

Yours sincerely,  
The developers
1to1software@gmail.com
